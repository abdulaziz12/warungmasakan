<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$router->post('/modern/','ModernController@create');
$router->get('/modern/','ModernController@read');
$router->post('/modern/{id}','ModernController@update');
$router->delete('/modern/{id}','ModernController@delete');
$router->get('/modern/{id}','ModernController@detail');


$router->post('tradisional/','TradisionalController@create');
$router->get('tradisional/','TradisionalController@read');
$router->post('tradisional/{id}','TradisionalController@update');
$router->delete('tradisional/{id}','TradisionalController@delete');
