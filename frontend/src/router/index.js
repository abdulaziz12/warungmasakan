import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Modern from '@/components/Modern'
import ModernForm from '@/components/ModernForm'
import Tradisional from '@/components/Tradisional'
import TradisionalForm from '@/components/TradisionalForm'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/modern',
      name: 'Modern',
      component: Modern
    },
    {
      path: '/modern/create',
      name: 'ModernCreate',
      component: ModernForm
    },
    {
      path: '/modern/:id',
      name: 'ModernEdit',
      component: ModernForm
    },
    {
      path: '/tradisional',
      name: 'Tradisional',
      component: Tradisional
    },
    {
      path: '/tradisional/create',
      name: 'TradisionalCreate',
      component: TradisionalForm
    },
    {
      path: '/tradisional/:id',
      name: 'TradisionalEdit',
      component: TradisionalForm
    }
  ]
})
