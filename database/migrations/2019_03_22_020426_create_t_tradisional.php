<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTTradisional extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_tradisional', function (Blueprint $table) {
          $table->increments('id');
          $table->string('nama_masakan', 50);
          $table->integer('harga')->length(100);
          $table->string('asal_daerah', 50);
          $table->text('keterangan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_tradisional');
    }
}
