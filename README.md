# Tugas Terakhir Pwd

Aplikasi berbasis web yang dibuat menggunakan lumen untuk backend dan 
vue.js untuk frontend. Aplikasi digunakan untuk menyimpan data penting seputar masakan modern & tradisional
Seperti : 

1. Masakan Modern
          
2. Masakan Tradisional
          


Your lumen project must use localhost;

Steps : 


## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

# UI APLIKASI

## 1. Beranda
Beranda ini digunakan sebagai identitas web dan hal pertama untuk menarik seseorang dalam sebuah web.
![Beranda](screenshot/1.png)

## 2. Read Data (Fitur Masakan Modern)
Sebuah page yang berisi data data anggota yang didalamnya dapat melakukan sebuah crud.
![ReadData](screenshot/2.png)

## 3. Create Data (Fitur Masakan Modern)
Sebuah page form untuk melakukan sebuah penambahan data anggota dan lansung bertambah kedatabase.
![ReadData](screenshot/3.png)

Sekian penjelasan singkat mengenai aplikas ini, semoga membantu