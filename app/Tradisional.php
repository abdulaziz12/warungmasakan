<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tradisional extends model
{
  public $table = 't_tradisional';

  protected $fillable =[
    'id','nama_masakan','harga','asal_daerah','keterangan'
  ];
}
?>
