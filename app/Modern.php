<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modern extends model
{
  public $table = 't_modern';

  protected $fillable =[
    'id','nama_masakan','harga','asal_daerah','keterangan'
  ];
}
?>
